
import socket
import sys
from json import JSONEncoder

import numpy as np
import pandas
import pandas as pd
from pandas import DataFrame

class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

class Site:

    def __init__(self, data):
        self.original_data = data
        self.data = data

    def length(self):
        return len(self.data)

def main(serverip, clientid):

    try:
        # create TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # retrieve local hostname
        local_hostname = socket.gethostname()

        server_address = (serverip, 23456)
        sock.connect(server_address)
        print("connecting to %s with %s" % (local_hostname, serverip))

        #data = np.array(read_data(clientid))
        data = (read_data(clientid))
        #print('data', data, type(data)) # DATA FRAME TYPE

        n=len(data)
        #print('data size', n)

        #path
        path = "data/data{}.csv".format(int(clientid))
        #path = "data/data{}_3.csv".format(int(clientid))

        #initialiser ( modifier) les donnees avec de bonne frequences
        data.loc[data['freq'] != 1/n, 'freq'] = 1/n

        #enregistrer les données ( mettre a jour)
        data.to_csv(path, index=False)

        #lire nouvellement les bonnes données
        data = (read_data(clientid))
        #print('initial data', data)

        sitei = Site(data)
        # --------------------------------------------------------------
        def send(message):
            sock.sendall(str(message).encode())
        print('waiting for messages..')
        t=0
        while True:
            message = sock.recv(1024)
            if not message:
                break
            print('Message:', message)
            tag, value = eval(message)
            result = ' '

            if tag == b'Haplotype Frequency':
                #vérificiation si itération 1  : GHF: freq=0
                GHF = pandas.read_table("GHF.csv", sep=',', header=0)
                #print (GHF)
                ########## a vérifier ceci####
                test = GHF.freq.isin([0.000]).all(axis=None)
                #test = GHF.loc[GHF['freq'] == 0.000, 'freq']
                #print('test', test)
                if test :
                    print('iteration t =', t)
                    res = sitei.data  # type data frame
                    result = res.values.tolist()  # type list
                    print('local haplotype frequency', result)
                else:
                    t=t+1
                    print("iteration", t)
                    #step 4: update the haplotype frequency
                    for y in GHF.itertuples():
                        data.loc[data.Haplotype == y.Haplotype, 'freq'] = y.freq
                    #print(data)

                    # step 5: Expectation step
                    l=list(data.itertuples())
                    #print (l, type(l))
                    E = []
                    # dénominateur
                    s=0
                    for i in range ( 0, len (l),2):
                        s=s+(2*l[i].freq*l[i+1].freq)
                    #print(s, '=s')

                    for i in range (0,len(l),2):
                        E.append((2*l[i].freq*l[i+1].freq)/s)
                    print('Expectation results', E)

                    # step 6: Maximisation step
                    NF=[]
                    j=0
                    for i in range (0, len(l), 2):
                        NF.append(round(2*l[i].freq*E[j],3))
                        #print(l[i], E[j])
                        NF.append(round(2*l[i+1].freq*E[j],3))
                        #print(l[i+1], E[j])
                        j=j+1
                    #print (NF)

                    data['freq'] = NF
                    print ('maximisation results', data)

                    #mettre a jour les données
                    path = "data/data{}.csv".format(int(clientid))
                    #path = "data/data{}_3.csv".format(int(clientid))
                    data.to_csv(path, index=False)

                    res = sitei.data  # type data frame
                    result = res.values.tolist()  # type list
                    print('local haplotype frequency for iteration',t, result)

            send(result)

    except Exception as e:
        #print(e)
        #raise e
        import traceback
        traceback.print_exc()
    finally:
        print("Closing connection")
        sock.close()


# 46, 126, 147

def read_data(clientid):
    path = "data/data{}.csv".format(int(clientid))
    #path = "data/data{}_3.csv".format(int(clientid))
    return pandas.read_table(path, sep=',', header=0)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('usage: ./client.py serverip clientid')
        sys.exit(1)
    serverip, clientid = sys.argv[1:]
    main(serverip, clientid)
