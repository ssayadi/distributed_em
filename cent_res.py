import pandas
import pandas as pd
import numpy as np

import time
start_time = time.time()
#donnée intiale
data = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/data_cent3.csv", sep=',', header=0)
#data = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/data_cent7_3.csv", sep=',', header=0)

# initialisation du GHF
GHF = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv", sep=',', header=0)
GHF=data
GHF.loc[GHF['freq'] >= 0,'freq'] = 0

GHF.to_csv("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv", index=False)
GHF = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv", sep=',', header=0)

#toute les possibilités des haplotypes
data.loc[data['freq'] != 1/len(data), 'freq'] = 1/len(data)
#print(data)

#somme des ferquences donnée intiale
s=sum(data.freq)
#print('somme des frequences donées initiales',s)


#g est le nombre des alleles en genotypes
g=2
#g=3

#condition d'arret
comp = False

#le nomnbre d'itération
t = 0

expectation = [0]

while (comp == False) :

    #cp = data.copy(deep=True)
    GHF = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv", sep=',', header=0)
    test = GHF.freq.isin([0.000]).all(axis=None)
    if test:
        print('iteration t =', t)
        #print ('test true')
        data['freq'] = round(data.groupby(['Haplotype']).transform('count') / len(data), 3)
        #print(data, 'step one')
    else:
        print('iteration t =', t)
        #print('test false')
        #print (data)
        print(GHF)
        print(GHF.drop_duplicates(subset="Haplotype"), 'avant calcul')
        # step 4: update the haplotype frequency
        for y in GHF.itertuples():
            data.loc[data.Haplotype == y.Haplotype, 'freq'] = y.freq
        #print(data, 'step 2')

        # step 5: Expectation step
        l = list(data.itertuples())
        #print (l, type(l))

        E = []
        d = []

        # dénominateur
        #s = 0
        for j in range(0, len(l), 2**g):
            dn = 0
            for i in range(j, j+2**g, 2):
                dn = dn + (2 * l[i].freq * l[i + 1].freq)
            d.append(dn)
        #print(d, "denominateur")

        k=0
        for j in range (0, len(l), 2**g):
            for i in range(j, j+2**g, 2):
                e=(2 * l[i].freq * l[i + 1].freq) / d[k]
                E.append(round(e,3))
            k=k+1
        print('Expectation results', E)
        expectation=E

        # step 6: Maximisation step
        NF = []
        j = 0
        for i in range(0, len(l), 2):
            NF.append(round(2 * l[i].freq * E[j], 3))
            # print(l[i], E[j])
            NF.append(round(2 * l[i + 1].freq * E[j], 3))
            # print(l[i+1], E[j])
            j = j + 1
        # print (NF)

        data['freq'] = NF
        print('maximisation results', data)

        #aggrégation de frequences
        data['freq'] = round(data.groupby(['Haplotype']).transform('mean'), 3)
    #print('Global Haplotype frequency', data.drop_duplicates(subset="Haplotype"))

    #somme des frequences
    s=sum(data.drop_duplicates(subset = "Haplotype").freq)
    print('somme des frequences données',s)

    c=0
    for i in range (0,len(expectation)):
        if expectation[i] >= 0.98:
            c=c+1

    # test sur la convergence de la valeur des expectation et de la somme des haplotypes freq
    if (round(s) != 1):
        break
    #################
    #if c >= len(expectation)/2:
        #break
    #if (round(s) != 1) or (c >= len(expectation)/2):
    #    break

    # normalization proposé par pag
    #data['freq'] = data['freq'] + (s-1) * (data['freq']/s)
    #print(data, 'data corr')
    #s = sum(data.drop_duplicates(subset="Haplotype").freq)
    #print('somme des frequences données', s)

    # compraison des valeurs pour la convergence
    GHF = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv", sep=',', header=0)
    print (GHF.drop_duplicates(subset="Haplotype"))
    dflisit=list(data.iterrows())
    GHFlisit = list(GHF.iterrows())
    comp=True
    th=0
    for i in range(len(GHFlisit)):
        diff=abs(round(GHFlisit[i][1]['freq'], 4) - round(dflisit[i][1]['freq'], 4))
    th=th+diff
    print ('SAE=',th)
    if th > 0.0001:
        data.to_csv("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv", index=False)
        comp = False
    else:
        comp = True
    t = t + 1


s=sum(GHF.drop_duplicates(subset = "Haplotype").freq)
GHF_mask=GHF['freq']!=0
filtred_GHF = GHF[GHF_mask]
print('filtred results', filtred_GHF.drop_duplicates(subset = "Haplotype"))
#print('results', GHF.drop_duplicates(subset = "Haplotype"))
print('somme des frequences données est égale à ',round(s))
print("--- %s seconds ---" % (time.time() - start_time))