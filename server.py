import socket
import sys
from threading import Thread
import sys
import json
import ast

import pandas
import pandas as pd


from threading import Thread

from pandas._libs import json


class ThreadWorker(Thread):
    def __init__(self, sock2):
        Thread.__init__(self)
        self.connection = sock2

    def send(self, tag, value):
        message = str((tag, value))
        self.connection.send(message.encode('utf-8'))

    def recv(self):
        BUFFER_SIZE = 1024
        res = []
        c = BUFFER_SIZE
        while c == BUFFER_SIZE:
            d = self.connection.recv(BUFFER_SIZE).decode()
            res.append(d)
            c = len(d)
        return ''.join(res)


    def recv_int(self):
        return int(self.recv())

    def recv_float(self):
        return float(self.recv())

    def recv_eval(self):
        x = self.recv()
        print('recv_eval', x)
        return eval(x)

class Site:

    def __init__(self, data):
        self.original_data = data
        self.data = data

    def length(self):
        return len(self.data)

data = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/data_cent3.csv", sep=',', header=0)
#data = pandas.read_table("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/data_cent5_3.csv", sep=',', header=0)

# initialisation du GHF
GHF = pandas.read_table("GHF.csv", sep=',', header=0)
GHF=data
GHF.loc[GHF['freq'] >= 0,'freq'] = 0
GHF.to_csv('GHF.csv', index=False)

GHF = pandas.read_table("GHF.csv", sep=',', header=0)
#print(GHF, 'GHF initial')

def main(serverip, nb_client):

    try:
        # create TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # retrieve local hostname
        local_hostname = socket.gethostname()

        # output hostname, domain name and IP address
        print("working on %s with %s" % (local_hostname, serverip))

        # bind the socket to the port 23456
        server_address = (serverip, 23456)
        print('starting up on %s port %s' % server_address)
        sock.bind(server_address)

        # listen for incoming connections (server mode) with one connection at a time
        sock.listen(1)

        clients = []

        for i in range(nb_client):
            # wait for a connection
            print('waiting for a connection')
            connection, client_address = sock.accept()
            # show who connected to us
            print('connection from', client_address)

            client = ThreadWorker(connection)
            client.setDaemon(True)
            # worker.start()

            clients.append(client)


        comp = False
        t = 0


        while comp==False:
            Haplotype_Frequency= [ ]
            haplo =[]
            fre = []

            import time
            start_time = time.time()

            # demander les haplotypes des cliens
            for client in clients:
                client.send(b'Haplotype Frequency', 0)
            # recevoir les haplotypes des cliens : step 1
            for client in clients:
                F = client.recv()  # F de type <class 'str'>
                Fg = eval(F)  # Convertir en list
                #print(F)
                Haplotype_Frequency.extend(Fg)
            #print(Haplotype_Frequency) #en list

            #N= len(Haplotype_Frequency)
            #print (N, 'number of haplotype frequency')

            #extraire  les haplotypes
            for element in Haplotype_Frequency:
                hap=element[0].split(',')[0]
                haplo.append(hap)
            #print(haplo)

            # extraire  les frequences
            for element in Haplotype_Frequency:
                fr = round(element[1],3) #.split(',')[1]
                fre.append(fr)
            #print(fre)

            # préparer le dataframe de tous les haplotypes de tous les sites
            df = pd.DataFrame(list(zip(haplo, fre)), columns=['Haplotype', 'freq'])
            # print('new data for iteration', t, df)


            s = sum(df.drop_duplicates(subset="Haplotype").freq)
            #print('somme des frequences', s)

            #calculer les frequences haplotypes globale: step 2
            #df['freq'] = round(df.groupby(['Haplotype']).transform('sum') / nb_client,3)
            GHF = pandas.read_table("GHF.csv", sep=',', header=0)
            test = GHF.freq.isin([0.000]).all(axis=None)
            # test = GHF.loc[GHF['freq'] == 0.000, 'freq']
            #print('test', test)
            if test:
                print('iteration t =', t)
                df['freq'] = round(df.groupby(['Haplotype']).transform('count') / len(df), 3)
                #print(df.drop_duplicates(subset="Haplotype"))
                print('Récupération et aggrégation des données des clients')
                print(df)
            else:
                print('iteration t =', t)
                df['freq'] = round(df.groupby(['Haplotype']).transform('mean'), 3)
                #print(df.drop_duplicates(subset = "Haplotype"))
                print(GHF.drop_duplicates(subset="Haplotype"))

            #haplotype ferquency global
            #print('Global Haplotype frequency for iteration', t, df.drop_duplicates(subset = "Haplotype"))
            s=sum(df.drop_duplicates(subset = "Haplotype").freq)
            #print('somme des frequences',s)

            #test pour condition d'arret (hypothése)(convergence)
            if (round(s) != 1):
                break

            ##########################################################################
            GHF = pandas.read_table("GHF.csv", sep=',', header=0)
            print('Last Global Haplotype frequency for t=', t-1, GHF)

            #compraison des valeurs pour la convergence
            dflisit=list(df.iterrows())
            GHFlisit = list(GHF.iterrows())
            comp=True
            th = 0
            for i in range(len(GHFlisit)):
                diff = abs(round(GHFlisit[i][1]['freq'], 4) - round(dflisit[i][1]['freq'], 4))
            th = th + diff
            print('SAE=', th)
            if th > 0.0001:
                df.to_csv("/Users/sirine/OneDrive/projet-PhD-git/phd-sirine-sayadi/data gouv/dishap/GHF.csv",index=False)
                comp = False
            else:
                comp = True
            t = t + 1
            #print(comp)
            ##########################################################################
        s = sum(GHF.drop_duplicates(subset="Haplotype").freq)
        GHF_mask = GHF['freq'] != 0
        filtred_GHF = GHF[GHF_mask]
        print('filtred results', filtred_GHF.drop_duplicates(subset="Haplotype"))
        #print('results', GHF.drop_duplicates(subset="Haplotype"))
        print('somme des frequences données est égale à ', round(s))
        print("--- %s seconds ---" % (time.time() - start_time))


    except Exception as e:

        import traceback
        traceback.print_exc()
        print(e)
    finally:
        for client in clients:
            client.connection.close()
        sock.close()
        print('Fermer les connections')

if __name__ == "__main__":

    if len(sys.argv) == 1:
        print('usage: python server.py nb_clients serverip')
        sys.exit(0)

    nb_client = int(sys.argv[1])
    if len(sys.argv) > 2:
        serverip = sys.argv[2]
    else:
        serverip = 'localhost'

    print('server ip:', serverip)
    print('number of clients:', nb_client)

    main(serverip, nb_client)


