import matplotlib.pyplot as plt

nbsite = [3,5,7,10,15,20]

#d_2=[3,5,7,10,15,20]
EM=[0.06, 0.08, 0.1, 0.12, 0.17,  0.23]
Dis_EM=[0.03, 0.04, 0.05, 0.07, 0.13, 0.18]


plt.plot(nbsite, Dis_EM, "r-",marker= 'o', markerfacecolor ='red', markersize=5, label="DisEM" )
plt.plot(nbsite, EM,"b:", marker=  '*', markerfacecolor ='blue', markersize=5, label="EM")
#plt.plot(nbsite, t_dpdis, "y--", marker=  '+',markerfacecolor = 'yellow', markersize=5, label="dpdis time(s)")

plt.legend()

plt.xlabel("Number of sites")
plt.ylabel("execution time (s)")

plt.show()